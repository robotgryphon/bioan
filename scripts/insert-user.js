const mongoose = require('mongoose');
import User from '../dist/models/user';

const host = "mongodb://localhost:27017/bioan";
mongoose.connect(host);
mongoose.Promise = global.Promise;
mongoose.connection
    .once('open', () => { 
        console.log("> Opened database connection.");
     })
    .on('error', (err) => {
        console.warn('error', err);
        assert.fail(true, true, err);
    });

User.create({
    username: "robotgryphon", 
    displayName: "Nano",
    email: "admin@bioan.me",
    password: "wouldntyouliketoknow"
}, (error, robotGryphon) => {
    if(!error) {
        console.log("Created user robotgryphon."); 
    }
});

mongoose.disconnect();