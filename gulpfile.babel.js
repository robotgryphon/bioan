const gulp = require('gulp')
const gulpif = require('gulp-if')
const babel = require('gulp-babel')
const del = require('del')
const path = require('path')
const polymer = require('polymer-build')
const cssSlam = require('css-slam').gulp;
const htmlMinifier = require('gulp-html-minifier');
const minify = require('gulp-minify');
const cleanCss = require('gulp-clean-css');
const mergeStream = require('merge-stream');
const stripComments = require('gulp-strip-comments');
const serviceWorker = polymer.generateServiceWorker;

const polyPath = './polymer.json';
const polyJSON = require(polyPath);
const project = new polymer.PolymerProject(polyJSON);

const debug = require('gulp-debug');

function waitFor(stream) {
  return new Promise((resolve, reject) => {
    stream.on('end', resolve);
    stream.on('error', reject);
  });
}

gulp.task('build', c => {
  console.log("Beginning build.");
  return new Promise((resolve, reject) => {

    del(["./build"]).then(_ => {
      let sourceSplitter = new polymer.HtmlSplitter();

      gulp.src('src/**/*')
        .pipe(babel())
        .pipe(gulp.dest('build/app'));

      let sources = project.sources()
        .pipe(sourceSplitter.split())
          .pipe(gulpif(/\.html$/, htmlMinifier()))
          .pipe(gulpif(/\.css$/, cssSlam()))
          .pipe(gulpif(/\.css$/, cleanCss()))
          .pipe(debug())
          // .pipe(gulpif(/\.js$/, babel()))
          // .pipe(stripComments())
          // .pipe(gulpif(/\.js$/, minify({ ignoreFiles: ['.min.js'] })))
          .pipe(sourceSplitter.rejoin());

      let depSplitter = new polymer.HtmlSplitter();
      let deps = project.dependencies()
        .pipe(depSplitter.split())
          .pipe(gulpif(/\.html$/, htmlMinifier()))
          .pipe(gulpif(/\.css$/, cssSlam()))
          .pipe(gulpif(/\.css$/, cleanCss()))
          // .pipe(gulpif(/\.js$/, minify({ ignoreFiles: ['.min.js'] })))
          .pipe(depSplitter.rejoin());
        
      let buildStream = mergeStream(sources, deps)
        .pipe(project.bundler({ stripComments: true }))
        .pipe(project.addPushManifest())
        .pipe(gulp.dest('build'));

      return waitFor(buildStream);
    })

    .then(_ => {
      console.log("Build complete.");
      resolve();
    });
  });
});

gulp.task('service-worker', function(callback) {
  var path = require('path');
  var swPrecache = require('sw-precache');
  import toArray from 'stream-to-array';
  const fileStream = mergeStream(project.sources(), project.dependencies());
  
  var files = toArray(fileStream)
    .then(file => file);

  console.log(files);

  swPrecache.write(`public/service-worker.js`, {
    staticFileGlobs: files,
    stripPrefix: "public",
    navigateFallback: '/index.html',
    navigateFallbackWhitelist: [/^(?!.*\.html$|\/data\/).*/]
  }, callback);
});