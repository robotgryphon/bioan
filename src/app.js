const path = require('path');
const compress = require('compression');
const cors = require('cors');
const helmet = require('helmet');
const bodyParser = require('body-parser');

const feathers = require('feathers');
const hooks = require('feathers-hooks');
const rest = require('feathers-rest');
const socketio = require('feathers-socketio');

const passport = require('passport');

// Services
import services from './services';

const app = feathers();

app.logger = require('winston');

let port = process.env.PORT || 3030;

if(process.env.DEBUG_MODE)
    app.logger.level = 'debug';

// Load app configuration

app
    .use(cors())
    .use(helmet())
    .use(compress())
    .use(bodyParser.json())
    .use(bodyParser.urlencoded({ extended: true }))
    // .use(favicon(path.join(__dirname, app.config.get('static.public'), 'favicon.ico')))

    // Host the public folder and static files
    .use("/bower_components", feathers.static(path.join(__dirname, process.env.DIR_BOWER)))
    .use("/node_modules", feathers.static(path.join(__dirname, process.env.DIR_NODE)))
    .use(feathers.static(path.join(__dirname, process.env.DIR_PUBLIC)))

    // Authentication stuff
    .use(passport.initialize())

    // Configure hooks, mongo, rest, and sockets
    .configure(hooks())
    .configure(rest())
    .configure(socketio(io => app.socketio = io))
    .configure(services);

app.get('*', (req, res) => { 
    res.sendFile(path.join(__dirname, process.env.DIR_PUBLIC, "index.html")); 
});

app.server = app.listen(port);

app.server.on('listening', _ => {
    app.logger.info(`Server started on port ${port}.`);
});

app.socketio.on('connection', (client) => {
    require("./socket-events")(client);
});

module.exports = app;
