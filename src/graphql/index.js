import graphqlHTTP from 'express-graphql';
import graphSchema from './schema';

module.exports = function () {
  const app = this;
  
  // Initialize our service with any options it requires
  app.use('/graphql', graphqlHTTP({
    schema: graphSchema,
    graphiql: true
  }));
};