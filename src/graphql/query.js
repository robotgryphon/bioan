import mongoose from 'mongoose';
import User from './types/user';

import {
    GraphQLObjectType,
    GraphQLString,
    GraphQLNonNull,
    GraphQLList
} from 'graphql';

/**
 * TODO:
 * 
 * Need to add the profile and profiles queries.
 * Profile query for fetching information from one profile, and limited by current AUTH headers.
 * Profiles query, which takes an owner id and returns a list of their profiles.
 * 
 */
const Query = new GraphQLObjectType({
    name: "BioanSchema",
    description: "Root Schema",
    fields: () => ({
        user: {
            type: User,
            description: "Fetch single user by their username.",
            args: {
                username: { type: new GraphQLNonNull(GraphQLString) }
            },

            resolve: function(source, { username }) {
                if(username) {
                    return mongoose.model('User').findOne({ username: username });
                } else {
                    return {};
                }
            }
        },

        users: {
            type: new GraphQLList(User),
            description: "Fetches all Bioan users.",
            resolve: (source, {}) => {
                return mongoose.model('User').find({});
            }
        }
    })
});

export default Query;