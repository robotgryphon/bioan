import {
    GraphQLInterfaceType,
    GraphQLString
} from 'graphql';
import GraphQLDate from 'graphql-date';

import User from './user';

const EntityInterface = new GraphQLInterfaceType({
    name: "EntityInterface",
    description: "Represents an entity on Bioan, whether it's a User, UserGroup, etc.",
    fields: () => ({
        id: {
            type: GraphQLString,
            required: true
        },

        created: {
            type: GraphQLDate,
            required: true
        }
    }),

    resolveType: (obj) => {
        if(obj.username) return User;
        
        return null;
    }
});

export default EntityInterface;