import {
    GraphQLObjectType,
    GraphQLString
} from 'graphql';

import GraphQLDate from 'graphql-date';
import Entity from './entity';

const ProfileSchema = new GraphQLObjectType({
    name: "Profile",
    description: "A Bioan character profile.",
    fields: () => ({
        slug: {
            type: GraphQLString,
            description: "Unique slug for URLs and editing.",
            required: true
        },

        created: {
            type: GraphQLDate,
            required: true
        },

        updated: {
            type: GraphQLDate,
            required: true,
            default: Date.now
        },

        owner: {
            type: Entity,
            required: true
        }
    })
});

export default ProfileSchema;