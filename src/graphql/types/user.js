import {
    GraphQLObjectType,
    GraphQLString,
    GraphQLList,
    GraphQLBoolean
} from 'graphql';

import GraphQLDate from 'graphql-date';

import mongoose from 'mongoose';
import Profile from './profile';
import Entity from './entity';

const UserPermission = new GraphQLObjectType({
    name: "UserPermission",
    description: "Grants a type of permission to a user on Bioan.",
    fields: () => ({
        name: {
            type: GraphQLString,
            required: true
        }
    })
});

const UserSchema = new GraphQLObjectType({
    name: "User",
    interfaces: [Entity],
    description: "Represents a Bioan user.",
    fields: () => ({
        id: {
            type: GraphQLString,
            required: true
        },

        created: {
            type: GraphQLDate,
            required: true
        },

        username: {
            type: GraphQLString,
            required: true
        },

        displayName: {
            type: GraphQLString,
            required: false
        },

        email: {
            type: GraphQLString,
            required: true
        },

        permissions: {
            type: new GraphQLList(UserPermission),
            required: true
        },

        profiles: {
            type: new GraphQLList(Profile),
            required: true,
            resolve: function({ user }) {
                return mongoose.model('Profile').find({ owner: user.id });
            }
        },

        private: {
            type: GraphQLBoolean,
            required: false
        }
    })
});

export default UserSchema;