const handler = require('feathers-errors/handler');
const notFound = require('feathers-errors/not-found');

const googleSignup = require('./google-signup');

module.exports = function () {
  // Add your custom middleware here. Remember, that
  // in Express the order matters, `notFound` and
  // the error handler have to go last.
  const app = this;

  console.log("Registering Google auth callback middleware.");
  app.get('/auth/google/callback', googleSignup(app));

  app.use(notFound());
  app.use(handler());
};