const passport = require('passport');
const Local = require('passport-local').Strategy;
const GoogleStrategy = require('passport-google-oauth20');

const User = require("../mongoose/models/user");

const googleScopes = [ 
  "https://www.googleapis.com/auth/userinfo.email",
  "https://www.googleapis.com/auth/userinfo.profile",
  "https://www.googleapis.com/auth/user.birthday.read" ];

module.exports = function () {
  const app = this;
  
  app.logger.info("Adding authentication bits.");

  passport.use('local-login', new Local((username, password, done) => {
    User.findOne({ username: username }, (err, user) => {
      if(err) return done(err);

      if(!user)
        return done(null, false, {
          message: 'auth.error.user_not_exists'
        });

      if(!user.validPassword(password))
        return done(null, false, {
          message: 'auth.user.bad_password'
        });

      return done(null, user);
    });
  }));

  app.post('/auth/local/login', (req, res, next) => {
    passport.authenticate('local', (err, user, info) => {
      console.log("Handling login request from user.");
      console.log("Username: ", req.body.username);
      
      if(err) {
        app.logger.error(err);
        next(err);
      }

      if(!user) {
        res.json({
          errored: true,
          message: info.message
        });
      }

      if(user) {
        console.log(user);

        res.json({
          errored: false,
          user: user
        });
      }

      res.end();
    })(req, res, next);
  });
};