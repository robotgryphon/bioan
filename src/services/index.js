const auth = require('./auth');
const graphql = require('../graphql');

module.exports = function () {
  const app = this; // eslint-disable-line no-unused-vars

  app.configure(auth);
  app.configure(graphql);
};