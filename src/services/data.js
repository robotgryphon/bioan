import { v1 as neo } from 'neo4j-driver';

const driver = neo.driver(process.env.DATABASE_HOST, neo.auth.basic(process.env.DATABASE_USER, process.env.DATABASE_PASS));

module.exports = driver;