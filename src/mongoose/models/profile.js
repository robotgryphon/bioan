import mongoose from 'mongoose';

const MongooseProfileSchema = mongoose.Schema({
    slug: {
        type: String,
        required: true
    },

    created: {
        type: Date,
        required: true
    },

    updated: {
        type: Date,
        default: Date.now,
        required: true
    },

    owner: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },

    private: {
        type: Boolean,
        required: false
    }
});

if(!mongoose.models['Profile']) mongoose.model('Profile', MongooseProfileSchema);
export const ProfileModel = mongoose.model('Profile');