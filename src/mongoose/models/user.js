import mongoose from 'mongoose';
import bcrypt from 'bcrypt';

const MongooseUserSchema = new mongoose.Schema({
    created: {
        type: Date,
        default: Date.now,
        required: true
    },

    username: {
        type: String,
        required: true
    },

    displayName: {
        type: String,
        required: false
    },

    email: {
        type: String,
        required: true
    },

    password: {
        type: String,
        required: true
    },

    roles: {
        type: [{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'UserRole'
        }]
    },

    profiles: {
        type: [{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Profile'
        }]
    },

    auth: {

        local: {
            password: {
                type: String,
                required: true
            }
        }
    }
});

MongooseUserSchema.methods.hashPassword = function(pass) {
    return bcrypt.hashSync(pass, bcrypt.genSaltSync(8), null);
}

MongooseUserSchema.methods.passwordValid = function(pass) {
    return bcrypt.compareSync(pass, this.auth.local.password);
}

if(!mongoose.models['User']) mongoose.model('User', MongooseUserSchema);
export const UserModel = mongoose.model('User');