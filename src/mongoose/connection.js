const mongoose = require('mongoose');

module.exports = function () {
  const app = this;

  const host = process.env.MONGO_HOST;
  mongoose.connect(host);
  mongoose.Promise = global.Promise;

  app.set('mongooseClient', mongoose);
};
