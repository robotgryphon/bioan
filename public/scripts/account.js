/**
 * Provides tools and methods for interacting with a Bioan account.
 * Uses Bioan.user heavily.
 */


import camelCase from 'lodash/camelCase';

export function roles(uid) {
    var dataRef = firebase.database().ref("user/" + uid);
    
    return new Promise((res, rej) => {
        // Get roles
        dataRef.child('roles').once('value').then(snap => {
            var roles = snap.val();
            res(roles);
        }).catch(err => rej(err));
    });        
}

function google() {
    if(this.signedIn) return false;
    var provider = new firebase.auth.GoogleAuthProvider();
    return firebase.auth().signInWithPopup(provider);
}

export function setDisplayName(newName) {
    if(!this.signedIn) {
        console.error("You have to be signed in!");
        return false;
    }

    if(!newName) {
        console.error("Name must not be null.");
        return false;
    }

    return this.updateRemoteUser({ displayName: newName });
}

export function updateRemoteUser(newData) {
    if(!this.signedIn) {
        console.error("You must be signed in to update your user.");
        return false;
    }
    
    if(newData) {
        var fbu = firebase.auth().currentUser;
        return fbu.updateProfile(newData);
    }

    return false;
}

export function unlockBadge(badgeType) {
    var actualBadgeType = camelCase(badgeType);

    console.log("Attempting to run " + actualBadgeType);

    console.log("You do not have the necessary requirements to unlock the badge.");
}